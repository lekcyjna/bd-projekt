#ifndef STEROWANIE_NAG
#define STEROWANIE_NAG

#include<iostream>
#include <unordered_map>
#include "json.hpp"

void trybInit();
void trybNormalny();

class wyjatek : public std::exception
{
  public:
    std::string tresc;
    std::string sciezka;
    std::string opis(){return tresc;}
    wyjatek(std::string s, std::string p){tresc=s; sciezka=p;}
};

class bladNiekrytyczny : public std::exception
{
  public:
    std::string tresc;
    bladNiekrytyczny(std::string s){tresc=s;}
};

#include "baza.hpp"

void opracowywaniePolecen(baza &polaczenie);

extern std::string sciezkaUruchomienia;
#endif
