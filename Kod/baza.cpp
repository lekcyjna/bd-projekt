#include "baza.hpp"
#include <fstream>

baza::baza(std::string opis):polaczenie(opis)
{}

void utworzBaze(baza &polaczenie)
{
//  std::cerr<<sciezkaUruchomienia+"Kod/stworzTabele";
  std::string pomoc;
  std::fstream plik;
  plik.open(sciezkaUruchomienia+"Kod/stworzTabele.sql");
  while(!plik.eof())
  {
    getline(plik,pomoc);
    if(pomoc=="")
      continue;
    pqxx::work tran(polaczenie.polaczenie);
    tran.exec(pomoc);
    tran.commit();
  }
  return;
}

void dodajUzyteId(long long id, char znak, pqxx::work &transakcja)
{
  transakcja.exec("INSERT INTO uzyteindeksy VALUES ("+std::to_string(id)+",'"+znak+"');");
}

void dodajLidera(baza &polaczenie, nlohmann::json wartosci)
{
  using json=nlohmann::json;
  using namespace std;
  json::iterator it=wartosci.begin();
  if(it.key()!="leader")
  {
    throw wyjatek("\nBłędne dane wejściowe. Przekazano: " + it.key() + " Powinno być:leader.\n","dodajLidera");
  }
  long long czas, id;
  string haslo;
  wartosci=it.value();
  czas=wartosci["timestamp"];
  id=wartosci["member"];
  haslo=wartosci["password"];
  pqxx::work zestaw(polaczenie.polaczenie);
  pqxx::result wynik = zestaw.exec("SELECT * FROM uzyteindeksy WHERE id=" +to_string(id)+";");
  if(wynik.size()!=0)
    throw bladNiekrytyczny("Id zadanego lidera istnieje.");
  zestaw.exec("INSERT INTO members VALUES("+to_string(id)+", crypt("+zestaw.quote(haslo)+",gen_salt('bf')),to_timestamp("+to_string(czas)+"),true);");
  dodajUzyteId(id, 'm', zestaw);
  zestaw.commit();
}

bool sprawdzCzyMemberIstnieje(long long member, pqxx::work &zestawPolecen)
//zwraca true jeśli istnieje, false jeśli nie istnieje i jest wolne
//bladNiekrytyczny jeśli jest zajęte, kończy program (wyjątek) jeśli istnieje ponad dwa wpisy
{
  using namespace pqxx;
  result wynik = zestawPolecen.exec("SELECT * FROM uzyteindeksy WHERE id=" + to_string(member)+";");
  if(wynik.size()==0)
  {
    return false;
  }
  if(wynik.size()>=2)
  {
    throw wyjatek ("Istnieje więcej niż jeden wpis dla id członka: " +to_string(member)+"\n","sprawdzCzyMemberIstnieje");
  }
  row wiersz=wynik[0];
  if(wiersz[1].as<std::string>()!="m")
  {
    throw bladNiekrytyczny("Id: " + to_string(member)+" zajęte.");
  }
  return true;
}

bool sprawdzCzyProjectIstnieje(long long project, pqxx::work &zestawPolecen)
//zwraca true jeśli istnieje, false jeśli nie istnieje i jest wolne
//bladNiekrytyczny jeśli jest zajęte, kończy program (wyjątek) jeśli istnieje ponad dwa wpisy
{
  using namespace pqxx;
  result wynik = zestawPolecen.exec("SELECT * FROM uzyteindeksy WHERE id=" + to_string(project)+";");
  if(wynik.size()==0)
  {
    return false;
  }
  if(wynik.size()>=2)
  {
    throw wyjatek ("Istnieje więcej niż jeden wpis dla id projektu: " +to_string(project)+"\n","sprawdzCzyProjectrIstnieje");
  }
  row wiersz=wynik[0];
  if(wiersz[1].as<std::string>()!="p")
  {
    throw bladNiekrytyczny("Id: " + to_string(project)+" zajęte.");
  }
  return true;
}

bool sprawdzCzyActionIstnieje(long long action, pqxx::work& zestawPolecen)
//zwraca true jeśli istnieje, false jeśli nie istnieje i jest wolne
//bladNiekrytyczny jeśli jest zajęte, kończy program (wyjątek) jeśli istnieje ponad dwa wpisy
{
  using namespace pqxx;
  result wynik = zestawPolecen.exec("SELECT * FROM uzyteindeksy WHERE id=" + to_string(action)+";");
  if(wynik.size()==0)
  {
    return false;
  }
  if(wynik.size()>=2)
  {
    throw wyjatek ("Istnieje więcej niż jeden wpis dla id akcji: " +to_string(action)+"\n","sprawdzCzyActionIstnieje");
  }
  row wiersz=wynik[0];
  if(wiersz[1].as<std::string>()!="a")
  {
    throw bladNiekrytyczny("Id: " + to_string(action)+" zajęte.");
  }
  return true;
}

bool sprawdzCzyOrganIstnieje(long long organ, pqxx::work &zestawPolecen)
//zwraca true jeśli istnieje, false jeśli nie istnieje i jest wolne
//bladNiekrytyczny jeśli jest zajęte, kończy program (wyjątek) jeśli istnieje ponad dwa wpisy
{
  using namespace pqxx;
  result wynik = zestawPolecen.exec("SELECT * FROM uzyteindeksy WHERE id=" + to_string(organ)+";");
  if(wynik.size()==0)
  {
    return false;
  }
  if(wynik.size()>=2)
  {
    throw wyjatek ("Istnieje więcej niż jeden wpis dla id organu władzy: " +to_string(organ)+"\n","sprawdzCzyOrganIstnieje");
  }
  row wiersz=wynik[0];
  if(wiersz[1].as<std::string>()!="o")
  {
    throw bladNiekrytyczny("Id: " + to_string(organ)+" zajęte.");
  }
  return true;
}

void dodajZwyklegoCzlonka(long long member, std::string haslo, long long czas, pqxx::work& zestawPolecen)
{
  using namespace std;
  zestawPolecen.exec("INSERT INTO members VALUES ("+to_string(member)+",crypt("+zestawPolecen.quote(haslo)+",gen_salt('bf')),to_timestamp("+to_string(czas)+"),false);");
  dodajUzyteId(member, 'm', zestawPolecen);
  return;
}

void dodajNowyProjekt(long long project, long long organ, pqxx::work& zestawPolecen)
{
  zestawPolecen.exec("INSERT INTO projekty VALUES ("+std::to_string(project)+","+std::to_string(organ)+");");
  dodajUzyteId(project, 'p', zestawPolecen);
  return;
}

bool sprawdzHaslo(long long member, std::string haslo, pqxx::work &zestawPolecen)
{
  pqxx::result hasloZaszyfrowane=zestawPolecen.exec("SELECT haslo FROM members WHERE id="+std::to_string(member)+";");
  pqxx::result pomoc=zestawPolecen.exec("SELECT crypt("+zestawPolecen.quote(haslo)+","+zestawPolecen.quote(hasloZaszyfrowane[0][0].as<std::string>())+");"); 
  if(hasloZaszyfrowane[0][0].as<std::string>()==pomoc[0][0].as<std::string>())
  {
    return true;
  }
  return false;
}

void sprawdzZamrozenie(long long member, long long czas, pqxx::work & zestawPolecen)
{
  using namespace pqxx;
  using namespace std;
  result wynik=zestawPolecen.exec("SELECT ostatnia_aktywnosc FROM members WHERE id="+to_string(member)+"and to_timestamp("+to_string(czas)+") <= ostatnia_aktywnosc + interval '1 year';");
  if(wynik.size()==0)
  {
    throw bladNiekrytyczny("Członek o id: " + to_string(member) + " jest zamrożony.");
  }
  return;
}

void uaktualnijCzlonka(long long member, long long czas, pqxx::work &zestawPolecen)
{
  using namespace std;
  zestawPolecen.exec("UPDATE members SET ostatnia_aktywnosc=to_timestamp("+to_string(czas)+") WHERE id="+to_string(member)+";");
  return;
}

bool sprawdzCzyCzlonekOddalGlos(long long member, long long action, pqxx::work &zestawPolecen)
{
  using namespace std;
  pqxx::result wynik=zestawPolecen.exec("SELECT * FROM glosy WHERE glosujacy="+to_string(member)+" and akcja="+to_string(action)+";");
  if(wynik.size()==0)
    return false;
  return true;
}

void polecenieSupportIProtest(baza &polaczenie, nlohmann::json opis, char typ)
{
  using namespace pqxx;
  using namespace std;
  using json=nlohmann::json;
  long long czas, member, action, project, organ;
  std::string haslo;
  czas=opis["timestamp"];
  member=opis["member"];
  action=opis["action"];
  project=opis["project"];
  haslo=opis["password"];
  work zestawPolecen(polaczenie.polaczenie);
  if(!sprawdzCzyMemberIstnieje(member, zestawPolecen))
  {
    dodajZwyklegoCzlonka(member, haslo, czas, zestawPolecen);
  }
  if(!sprawdzHaslo(member, haslo, zestawPolecen))
  {
    throw bladNiekrytyczny("Podano błędne hasło.");
  }
  sprawdzZamrozenie(member,czas, zestawPolecen);
  if(sprawdzCzyActionIstnieje(action, zestawPolecen))
  {
    throw bladNiekrytyczny("Akcja o podanym id już istnieje.");
  }
  if(!sprawdzCzyProjectIstnieje(project, zestawPolecen))
  {
    organ=opis["authority"];
    if(!sprawdzCzyOrganIstnieje(organ, zestawPolecen))
    {
      dodajUzyteId(organ, 'o', zestawPolecen);
    }
    dodajNowyProjekt(project, organ, zestawPolecen);
  }
  zestawPolecen.exec("INSERT INTO akcje VALUES ("+to_string(action)+","+to_string(project)+",'"+typ+"',"+to_string(member)+");");
  dodajUzyteId(action, 'a', zestawPolecen);
  uaktualnijCzlonka(member, czas, zestawPolecen);
  zestawPolecen.commit();
  return;
}

bool czyLider(long long member, pqxx::work &zestawPolecen)
{
  pqxx::result wynik = zestawPolecen.exec("SELECT lider FROM members WHERE id="+ std::to_string(member)+";");
  return wynik[0][0].as<bool>();
}

nlohmann::json polecenieActions(baza &polaczenie, nlohmann::json opis)
{
  using namespace std;
  using namespace pqxx;
  using json=nlohmann::json;
  json odpowiedz;
  work zestawPolecen(polaczenie.polaczenie);
  long long czas, member, projekt, organ;
  short wystTyp, wystProjekt, wystOrgan;
  string haslo, typ;
  czas=opis["timestamp"];
  member=opis["member"];
  haslo=opis["password"];
  wystTyp=opis.count("type");
  wystOrgan=opis.count("authority");
  wystProjekt=opis.count("project");
  if(!sprawdzCzyMemberIstnieje(member, zestawPolecen))
  {
    throw bladNiekrytyczny("Lider o podanym id nie istnieje.");
  }
  sprawdzZamrozenie(member, czas, zestawPolecen);
  if(!czyLider(member, zestawPolecen))
    throw bladNiekrytyczny("Brak uprawnień do wykonania funkcji actions.");
  if(!sprawdzHaslo(member, haslo, zestawPolecen))
    throw bladNiekrytyczny("Błędne hasło.");
  string polecenie="SELECT akcje.id, typ, projekt_id, organ, count(czyza) FROM akcje join projekty on projekt_id=projekty.id left join glosy on akcje.id=akcja WHERE ";
  if(wystTyp==1)
  {
    typ=opis["type"];
    if(typ=="support")
    {
      polecenie+="typ='s' and ";
    }
    else
    {
      polecenie+="typ='p' and ";
    }
  }
  if(wystOrgan==1)
  {
    organ=opis["authority"];
    polecenie+="organ=" + to_string(organ)+" and ";
  }
  else if (wystProjekt==1)
  {
    projekt=opis["project"];
    polecenie+="projekt_id="+to_string(projekt)+" and ";
  }
  string poleceniePrzeciw=polecenie;
  string polecenieWszystkieAkcje=polecenie;
  polecenie+="czyza=true group by akcje.id, typ, projekt_id, organ order by akcje.id;";
  poleceniePrzeciw+="czyza=false group by akcje.id, typ, projekt_id, organ order by akcje.id;";
  polecenieWszystkieAkcje+="true group by akcje.id, typ, projekt_id, organ order by akcje.id;";
  result wynikWszystkie=zestawPolecen.exec(polecenieWszystkieAkcje);
  result wynikZa=zestawPolecen.exec(polecenie);
  result wynikPrzeciw=zestawPolecen.exec(poleceniePrzeciw); 
  int liczbaAkcji=wynikWszystkie.size();
  int liczbaZa=wynikZa.size();
  int liczbaPrzeciw=wynikPrzeciw.size();
  int itZa=0, itPrzeciw=0;
  for(int i=0;i<liczbaAkcji;i++)
  {
    json pomoc;
    long long idAkcji=wynikWszystkie[i][0].as<long long>();
    pomoc.push_back(wynikWszystkie[i][0].as<long long>());
    pomoc.push_back(wynikWszystkie[i][1].as<string>());
    pomoc.push_back(wynikWszystkie[i][2].as<long long>());
    pomoc.push_back(wynikWszystkie[i][3].as<long long>());
    while(itZa<liczbaZa && wynikZa[itZa][0].as<long long>()<idAkcji)
    {
      itZa++;
    }
    if(itZa<liczbaZa && wynikZa[itZa][0].as<long long>()==idAkcji)
    {
      pomoc.push_back(wynikZa[itZa][4].as<long long>());
    }
    else
    {
      pomoc.push_back(0);
    }
    while(itPrzeciw<liczbaPrzeciw && wynikPrzeciw[itPrzeciw][0].as<long long>()<idAkcji)
    {
      itPrzeciw++;
    }
    if(itPrzeciw<liczbaPrzeciw && wynikPrzeciw[itPrzeciw][0].as<long long>()==idAkcji)
    {
      pomoc.push_back(wynikPrzeciw[itPrzeciw][4].as<long long>());
    }
    else
    {
      pomoc.push_back(0);
    }
    odpowiedz.push_back(pomoc);
  }
  uaktualnijCzlonka(member,czas,zestawPolecen);
  zestawPolecen.commit();
  return odpowiedz;
}

nlohmann::json polecenieProject(baza &polaczenie, nlohmann::json opis)
{
  using namespace std;
  using namespace pqxx;
  using json=nlohmann::json;
  work zestawPolecen(polaczenie.polaczenie);
  long long member, czas, organ;
  string haslo;
  short wystOrgan;
  member=opis["member"];
  czas=opis["timestamp"];
  haslo=opis["password"];
  wystOrgan=opis.count("authority");
  if(!sprawdzCzyMemberIstnieje(member, zestawPolecen))
  {
    throw bladNiekrytyczny("Lider o takim id nie istnieje.");
  }
  sprawdzZamrozenie(member, czas, zestawPolecen);
  if(!czyLider(member, zestawPolecen))
    throw bladNiekrytyczny("Członek o podanym id nie jest liderem.");
  if(!sprawdzHaslo(member, haslo, zestawPolecen))
    throw bladNiekrytyczny("Podano błędne hasło lidera.");
  string polecenie="SELECT id, organ FROM projekty WHERE ";
  if(wystOrgan!=0)
  {
    organ=opis["authority"];
    polecenie+="organ=" + to_string(organ) + " and ";
  }
  polecenie+="true order by id;";
  result wynik=zestawPolecen.exec(polecenie); 
  int rozmiar=wynik.size();
  json odpowiedz;
  for(int i=0;i<rozmiar;i++)
  {
    json pomoc;
    pomoc.push_back(wynik[i][0].as<long long>());
    pomoc.push_back(wynik[i][1].as<long long>());
    odpowiedz.push_back(pomoc);
  }
  uaktualnijCzlonka(member, czas, zestawPolecen);
  zestawPolecen.commit();
  return odpowiedz;
}

void zmienStosunekGlosow(pqxx::work& zestawPolecen, long long akcja, bool typ)
{
  //dodaj autorowi akcji 1 głos typu "typ"
  using namespace pqxx;
  using namespace std;
  string polecenie="SELECT autor FROM akcje WHERE id="+to_string(akcja)+";";
  result wynik=zestawPolecen.exec(polecenie);
  if(wynik.size()>1)
    throw wyjatek("Jest wiele akcji o tym samym id.","zmienStosunekGlosow");
  if(wynik.size()==0)
    throw bladNiekrytyczny("Nie ma akcji o takim id.");
  long long autor=wynik[0][0].as<long long>();
  if(typ)
  {
    //upvote
    polecenie="UPDATE members SET upvote=upvote+1, roznica=roznica+1 WHERE id="+to_string(autor)+";";
  }
  else
  {
    //downvote
    polecenie="UPDATE members SET downvote=downvote-1, roznica=roznica-1 WHERE id="+to_string(autor)+";";
  }
  zestawPolecen.exec(polecenie);
  return;
} 

void polecenieUpIDownvote(baza &polaczenie, nlohmann::json opis, bool typ)
  //jeśli typ==true to upvote w.p.p downvote
{
  using namespace std;
  using namespace pqxx;
  using json=nlohmann::json;
  long long czas, member, akcja;
  string haslo;
  czas=opis["timestamp"];
  member=opis["member"];
  akcja=opis["action"];
  haslo=opis["password"];
  work zestawPolecen(polaczenie.polaczenie);
  if(!sprawdzCzyMemberIstnieje(member, zestawPolecen))
  {
    dodajZwyklegoCzlonka(member, haslo, czas, zestawPolecen);
  }
  sprawdzZamrozenie(member, czas, zestawPolecen);
  if(!sprawdzHaslo(member, haslo, zestawPolecen))
  {
    throw bladNiekrytyczny("Podano błedne hasło dla członka do funkcji up/down vote.");
  }
  if(!sprawdzCzyActionIstnieje(akcja, zestawPolecen))
  {
    throw bladNiekrytyczny("Próba oddania glosu na akcje która nie istnieje.");
  }
  if(sprawdzCzyCzlonekOddalGlos(member, akcja, zestawPolecen))
    throw bladNiekrytyczny("Próba oddania kolejnego głosu na tą samą akcję.");
  //wiemy w tym miejscu że:
  //członek istnieje, jest niezamrożony, podał poprawne hasło i nie oddał głosu na tą akcje
  //akcja istnie
  string polecenie="INSERT INTO glosy(czas, glosujacy, akcja, czyza) VALUES (to_timestamp("+to_string(czas)+"),"+to_string(member)+","+to_string(akcja)+",";
  if(typ)
  {
    polecenie+="true);";
  }
  else
  {
    polecenie+="false);";
  }
  zestawPolecen.exec(polecenie);
  zmienStosunekGlosow(zestawPolecen,akcja,typ);
  uaktualnijCzlonka(member, czas, zestawPolecen);
  zestawPolecen.commit();
  return;
}

nlohmann::json polecenieVotes(baza& polaczenie, nlohmann::json opis)
{
  using namespace std;
  using namespace pqxx;
  using json=nlohmann::json;
  json odpowiedz;
  work zestawPolecen(polaczenie.polaczenie);
  long long czas, member, akcja, projekt;
  string haslo;
  czas=opis["timestamp"];
  member=opis["member"];
  haslo=opis["password"];
  short liczbaAkcja=opis.count("action"), liczbaProjekt=opis.count("project");

  if(!sprawdzCzyMemberIstnieje(member, zestawPolecen))
    throw bladNiekrytyczny("Podany członek nie istnieje.");
  if(!czyLider(member, zestawPolecen))
    throw bladNiekrytyczny("Podany członek nie jest liderem.");
  if(!sprawdzHaslo(member, haslo, zestawPolecen))
    throw bladNiekrytyczny("Podano błedne hasło.");
  sprawdzZamrozenie(member, czas, zestawPolecen);

  string polecenie="SELECT id FROM members ORDER BY id;";
  string poleceniePrzeciw="SELECT glosujacy, count(czyza) FROM glosy join akcje on akcja=akcje.id WHERE czyza=false and ";
  string polecenieZa="SELECT glosujacy, count(czyza) FROM glosy join akcje on akcja=akcje.id WHERE czyza=true and ";

  if(liczbaAkcja!=0)
  {
    akcja=opis["action"];
    if(!sprawdzCzyActionIstnieje(akcja, zestawPolecen))
      throw bladNiekrytyczny("Podana akcja nie istnieje.");
    polecenieZa+="akcja="+to_string(akcja)+" and ";
    poleceniePrzeciw+="akcja="+to_string(akcja)+" and ";
  }
  else if(liczbaProjekt!=0)
  {
    projekt=opis["project"];
    if(!sprawdzCzyProjectIstnieje(projekt, zestawPolecen))
      throw bladNiekrytyczny("Podany projekt nie istnieje.");
    polecenieZa+="projekt_id=" + to_string(projekt) +" and ";
    poleceniePrzeciw+="projekt_id=" + to_string(projekt) + " and ";
  }

  poleceniePrzeciw+="true GROUP BY glosujacy ORDER BY glosujacy;";
  polecenieZa+="true GROUP BY glosujacy ORDER BY glosujacy;";

  result wynik=zestawPolecen.exec(polecenie);
  result wynikZa=zestawPolecen.exec(polecenieZa);
  result wynikPrzeciw=zestawPolecen.exec(poleceniePrzeciw);
  int liczbaOsob=wynik.size();
  int itZa=0, itPrzeciw=0;
  int liczbaZa=wynikZa.size(), liczbaPrzeciw=wynikPrzeciw.size();
  for(int i=0;i<liczbaOsob;i++)
  {
    json pomoc;
    long long osoba=wynik[i][0].as<long long>();
    pomoc.push_back(osoba);
    while(itZa<liczbaZa && wynikZa[itZa][0].as<long long>()<osoba)
    {
      itZa++;
    }
    if(itZa<liczbaZa && wynikZa[itZa][0].as<long long>()==osoba)
    {
      pomoc.push_back(wynikZa[itZa][1].as<long long>());
    }
    else
    {
      pomoc.push_back(0);
    }
    while(itPrzeciw<liczbaPrzeciw && wynikPrzeciw[itPrzeciw][0].as<long long>()<osoba)
    {
      itPrzeciw++;
    }
    if(itPrzeciw<liczbaPrzeciw && wynikPrzeciw[itPrzeciw][0].as<long long>()==osoba)
    {
      pomoc.push_back(wynikPrzeciw[itPrzeciw][1].as<long long>());
    }
    else
    {
      pomoc.push_back(0);
    }
    odpowiedz.push_back(pomoc);
  }
  uaktualnijCzlonka(member, czas, zestawPolecen);
  zestawPolecen.commit();
  return odpowiedz;
}

nlohmann::json polecenieTrolls(baza& polaczenie, nlohmann::json opis)
{
  using json=nlohmann::json;
  using namespace std;
  using namespace pqxx;
  work zestawPolecen(polaczenie.polaczenie);
  json odpowiedz;

  long long czas;
  czas=opis["timestamp"];
  string polecenie="SELECT id, upvote, -downvote, not ostatnia_aktywnosc+interval '1 year'<=to_timestamp("+to_string(czas)+") FROM members WHERE roznica<0 ORDER BY roznica;";
  result wynik=zestawPolecen.exec(polecenie);
  int rozmiar=wynik.size();
  for(int i=0;i<rozmiar;i++)
  {
    json pomoc;
    pomoc.push_back(wynik[i][0].as<long long>());
    pomoc.push_back(wynik[i][1].as<long long>());
    pomoc.push_back(wynik[i][2].as<long long>());
    pomoc.push_back(wynik[i][3].as<bool>());
    odpowiedz.push_back(pomoc);
  }
  zestawPolecen.commit();
  return odpowiedz;
}



