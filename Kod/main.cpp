#include"sterowanie.hpp"

std::string sciezkaUruchomienia;

int main(int argc, char* argv[])
{
  try
  {
    using namespace std;
    if(argc>2)
    {
      cerr<<"Uruchomiono program z błędną liczbą argumentów.\n";
      cerr<<"Poprawne użycie:\n";
      cerr<<"Bez argumentów zaloguj się jako użytkownik normalny.\n";
      cerr<<"--init - zaloguj się jako twórca.\n";
      return 1;
    }
    sciezkaUruchomienia=string(argv[0]);
    sciezkaUruchomienia.erase(sciezkaUruchomienia.rfind("/")+1);
    if(argc==2)
    {
      if(string(argv[1])!="--init")
      {
        cerr<<"Podano błędny 2. argument.\n";
        return 1;
      }
      else
      {
        trybInit();
      }
    }
    else
    {
      trybNormalny();
    }
    return 0;
  }
  catch(wyjatek blad)
  {
    std::cerr<<blad.tresc<<"\n";
    std::cerr<<blad.sciezka<<"\n";
    throw std::move(blad);
  }
}

