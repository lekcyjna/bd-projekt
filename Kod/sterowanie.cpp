#include "sterowanie.hpp"


std::string stworzStringOtwarcia(nlohmann::json wartosci)
{
  using namespace std;
  nlohmann::json::iterator it=wartosci.begin();
  string nazwa, haslo, login;
  if(it.key()=="database")
  {
    nazwa=it.value();
    it++;
  }
  else
    throw wyjatek("\nBłędne pierwsze pole w open.\n","trybInit");
  if(it.key()=="login")
  {
    login=it.value();
    it++;
  }
  else
    throw wyjatek("\nBłędne drugie pole w open.\n","trybInit");
  if(it.key()=="password")
  {
    haslo=it.value();
  }
  else
    throw wyjatek("\nBłędne drugie pole w open.\n","trybInit");
  return "dbname=" + nazwa + " user=" + login + " password=" + haslo + " host=localhost";
}

void tworzenieLiderow(baza &polaczenie)
{
  using namespace std;
  using json =nlohmann::json;
  json wartosci;
  json komunikat_OK;
  komunikat_OK["status"]="OK";
  json komunikat_ERROR;
  komunikat_ERROR["status"]="ERROR";
  string opis;
  while(!cin.eof())
  {
    getline(cin,opis);
  //  cerr<<opis;
    if(opis=="")
    {
      break;
    }
    wartosci=json::parse(opis);
    try
    {
      dodajLidera(polaczenie,wartosci);
      cout<<komunikat_OK<<"\n";
    }
    catch(bladNiekrytyczny blad)
    {
      json kopia=komunikat_ERROR;
      kopia["debug"]=blad.tresc;
      cout<<kopia<<"\n";
    }
  }
}

void trybInit()
{
  using json=nlohmann::json;
  baza* polaczenie;
  using namespace std;
  string linia;
  json polecenie;
  getline(cin, linia);
  polecenie=json::parse(linia);
  json::iterator it=polecenie.begin();
  if(it.key()=="open")
  {
    json wartosci=it.value();
    string opisPolaczenia=stworzStringOtwarcia(wartosci);
    polaczenie = new baza(opisPolaczenia);
    json komunikat_OK;
    komunikat_OK["status"]="OK";
    cout<<komunikat_OK<<"\n";
  }
  else
  {
    json komunikat_ERROR;
    komunikat_ERROR["status"]="ERROR";
    cout<<komunikat_ERROR<<"\n";
    throw wyjatek("\nW pierwszej linii wejścia dla trybu init nie ma polecenia open.\n","trybInit");
  }
  utworzBaze(*polaczenie);
  tworzenieLiderow(*polaczenie);
}

void trybNormalny()
{
  using json=nlohmann::json;
  baza *polaczenie;
  using namespace std;
  json wiersz;
  string linia;
  getline(cin, linia);
  wiersz=json::parse(linia);
  json::iterator it;
  if((it=wiersz.find("open"))==wiersz.end())
  {
    json komunikat_ERROR;
    komunikat_ERROR["status"]="ERROR";
    cout<<komunikat_ERROR<<"\n";
    throw wyjatek("\nW pierwszej linii wejścia dla trybu app nie ma polecenia open.\n","trybNormalny");
  }
  string opisPolaczenia=stworzStringOtwarcia(it.value());
  polaczenie=new baza(opisPolaczenia);
  json komunikat_OK;
  komunikat_OK["status"]="OK";
  cout<<komunikat_OK<<"\n";
  opracowywaniePolecen(*polaczenie);
  return;
}

void opracowywaniePolecen(baza &polaczenie)
{
  using namespace std;
  using json=nlohmann::json;
  json wiersz;
  string linia;
  json::iterator it;
  while(!cin.eof())
  {
    getline(cin, linia);
    if(linia=="")
    {
      break;
    }
    wiersz=json::parse(linia);
    it=wiersz.begin();
    string nazwaPolecenia=it.key();
    if(nazwaPolecenia=="support")
    {
      try
      {
        polecenieSupportIProtest(polaczenie, it.value(),'s');
        json komunikat_OK;
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["status"]="ERROR";
        komunikat_ERROR["debug"]=blad.tresc;
        cout<<komunikat_ERROR<<"\n";
      }
    }
    else if (nazwaPolecenia=="protest")
    {
      try
      {
        polecenieSupportIProtest(polaczenie, it.value(),'p');
        json komunikat_OK;
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["status"]="ERROR";
        komunikat_ERROR["debug"]=blad.tresc;
        cout<<komunikat_ERROR<<"\n";
      }
    }
    else if (nazwaPolecenia=="upvote")
    {
      try
      {
        polecenieUpIDownvote(polaczenie, it.value(),true);
        json komunikat_OK;
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["status"]="ERROR";
        komunikat_ERROR["debug"]=blad.tresc;
        cout<<komunikat_ERROR<<"\n";
      }
    }
    else if (nazwaPolecenia=="downvote")
    {
      try
      {
        polecenieUpIDownvote(polaczenie, it.value(),false);
        json komunikat_OK;
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["status"]="ERROR";
        komunikat_ERROR["debug"]=blad.tresc;
        cout<<komunikat_ERROR<<"\n";
      }
    }
    else if (nazwaPolecenia=="actions")
    {
      try
      {
        json komunikat_OK;
        komunikat_OK["data"]=polecenieActions(polaczenie, it.value());
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["status"]="ERROR";
        komunikat_ERROR["debug"]=blad.tresc;
        cout<<komunikat_ERROR<<"\n";
      }

    }
    else if (nazwaPolecenia=="projects")
    {
     try
      {
        json komunikat_OK;
        komunikat_OK["data"]=polecenieProject(polaczenie, it.value());
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["debug"]=blad.tresc;
        komunikat_ERROR["status"]="ERROR";
        cout<<komunikat_ERROR<<"\n";
      }
    }
    else if (nazwaPolecenia=="votes")
    {
      try
      {
        json komunikat_OK;
        komunikat_OK["data"]=polecenieVotes(polaczenie, it.value());
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["debug"]=blad.tresc;
        komunikat_ERROR["status"]="ERROR";
        cout<<komunikat_ERROR<<"\n";
      }

    }
    else if (nazwaPolecenia=="trolls")
    {
      try
      {
        json komunikat_OK;
        komunikat_OK["data"]=polecenieTrolls(polaczenie, it.value());
        komunikat_OK["status"]="OK";
        cout<<komunikat_OK<<"\n";
      }
      catch(bladNiekrytyczny blad)
      {
        json komunikat_ERROR;
        komunikat_ERROR["debug"]=blad.tresc;
        komunikat_ERROR["status"]="ERROR";
        cout<<komunikat_ERROR<<"\n";
      }

    }
    else
    {
      throw wyjatek("Nie rozpoznano polecenia: "+wiersz.dump()+ "\nSprawdzano nazwę polecenia: "+nazwaPolecenia,"opracowywaniePolecen");
    }
  }
}












