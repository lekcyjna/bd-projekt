
create role app with login encrypted password 'qwerty';

--poniższe linie muszą być przed tworzeniem tabel
alter default privileges for user init grant insert, select, update on tables to app;
alter default privileges for user init grant execute on functions to app;

-- s-support, p-protest
create domain typ_akcji as char(1) check (value in ('s','p'));
-- m-member, a-akcja, p-projekt, o-organ
create domain typ_id as char(1) check (value in ('m', 'a', 'p', 'o'));

--upvote i downvote to suma wszystkich głosów za i przeciw oddanych na akcje stworzone przez member
create table members (id integer primary key, haslo text not null, ostatnia_aktywnosc timestamp not null, lider bool default false, roznica int default 0, upvote int default 0, downvote int default 0);
create table projekty(id int primary key, organ int not null);
create table akcje(id int primary key, projekt_id int references projekty(id) not null, typ typ_akcji not null, autor int not null);
create table glosy(czas timestamp primary key, glosujacy int references members(id) not null, akcja int references akcje(id) not null, czyZa bool not null);
create table uzyteIndeksy(id int primary key, typ typ_id not null);

create index members_idx on members using hash (id);
create index indeksy_idx on uzyteIndeksy using hash (id);


