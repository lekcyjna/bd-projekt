#ifndef BAZA_NAG
#define BAZA_NAG

#include <pqxx/pqxx>


class baza
{
  public:
  pqxx::connection polaczenie;
  baza() =delete;
  baza(std::string opis);
};

void utworzBaze(baza &polaczenie);

#include "sterowanie.hpp"

void dodajLidera(baza &polaczenie, nlohmann::json wartosci);
void polecenieSupportIProtest(baza &polaczenie, nlohmann::json opis, char typ);
nlohmann::json polecenieActions(baza &polaczenie, nlohmann::json opis);
nlohmann::json polecenieProject(baza &polaczenie, nlohmann::json opis);
void polecenieUpIDownvote(baza &polaczenie, nlohmann::json opis, bool typ);
nlohmann::json polecenieVotes(baza &polaczenie, nlohmann::json opis);
nlohmann::json polecenieTrolls(baza &polaczenie, nlohmann::json opis);

extern std::string sciezkaUruchomienia;
#endif
