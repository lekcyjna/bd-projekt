CC=g++
CFLAGS=-std=c++14 -Wall -Wextra -I./Kod/include/
LFLAGS=-lpqxx -lpq -L./Kod/lib

#nazwa programu
NAZWA=ParteiFuerKaltbuetigenwustendrachen
#pliki źródłowe
PLIKIC=./Kod/main.cpp ./Kod/sterowanie.cpp ./Kod/baza.cpp
NAGLOWKI=./Kod/sterowanie.hpp ./Kod/baza.hpp
#nazwy modułów
MODULY=./Kod/sterowanie.o ./Kod/main.o ./Kod/baza.o

YOU:$(PLIKIC) $(NAZWA)

$(NAZWA):$(MODULY)
	$(CC) $(CFLAGS) $(MODULY) -o $(NAZWA) $(LFLAGS)

%.o: %.cpp $(NAGLOWKI)
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(MODULY) $(NAZWA)
	rm -f *~ ./Kod/*~
